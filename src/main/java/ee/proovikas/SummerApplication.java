package ee.proovikas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SummerApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(SummerApplication.class, args);
    }

}
