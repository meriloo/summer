package ee.proovikas;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PreDestroy;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;


@RestController
public class Controller {

    private static long sum;
    private static CountDownLatch countDownLatch = new CountDownLatch(1);
    private static AtomicInteger waitingThreadCounter= new AtomicInteger(0);

    @PostMapping("/")
    public @ResponseBody String getInput(@RequestBody String query) {
        String response = "";
        query = StringUtils.removeEnd(query, "=");

        if(query.matches("-?\\d+")){
            long result = Long.parseLong(query);
            sum = sum + result;
            try {
                waitingThreadCounter.incrementAndGet();
                countDownLatch.await();
                response = "Sum is: " + sum + "\n";
                waitingThreadCounter.decrementAndGet();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else if ("end".equals(query)) {
            countDownLatch.countDown();
            countDownLatch = new CountDownLatch(1);
            response = "Good bye Dave \n";
            response = "Sum is: " + sum + "\n" + response;
        } else {
            response = "I'm sorry Dave but you must post a number \n";
        }

        if (waitingThreadCounter.get() == 0) {
            sum = 0;
        }

        return response;
    }
}