# Eeldused

*       JDK8
*       Gradle 4.x


## Tomcat'i confimine

*   muudame serveri pordi ning threadpooli ära

        sudo nano /opt/tomcat/conf/server.xml
        
*   muuda HTTP <Connector>

        <Connector port="8080" protocol="HTTP/1.1"  -->
             
        <Connector executor="tomcatThreadPool" port="1337" protocol="HTTP/1.1"

*   muuda või lisa <Executor>

        <Executor name="tomcatThreadPool" 
                  namePrefix="catalina-exec-"
                  maxThreads="21" 
                  minSpareThreads="1"/>

### War faili ehitamine

*   rakenduse kodukataloogis käivitame 

        ./gradlew bootWar  --> /build/lib/summer-1.0.war

#### Deploy
#   eeldatud on, et tomcat on kaustas /opt/tomcat.

*   kui vaja, siis koristame vana ROOT.war ja ROOT kasuta:

        /opt/tomcat/bin/shutdown.sh --> tomcat maha
        
        rm /opt/tomcat/webapps/ROOT.war --> vana ROOT.war maha
        
        rm -rf /opt/tomcat/webapps/ROOT --> ROOT kaust maha

*   rakenduse /build/lib/ kaustast käivitame järgmised käsud:

        mv summer-1.0.war ROOT.war --> nimetame oma war faili ümber
        
        cp ROOT.war /opt/apache-tomcat/webapps --> kopeerime tomcat'i webappside kausta
        
        /opt/apache-tomcat/bin/startup.sh --> tomcat püsti

##### kasutamine

*   kui tomcat on püsti tulnud, siis saab talle postida curl-iga:

         curl -d 1 http://localhost:1337/


        
